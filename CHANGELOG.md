
## 0.4.3 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!32

---

## 0.4.2 [06-11-2021]

* Removed deprecated dependency and Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!31

---

## 0.4.1 [03-04-2021]

* Patch/lb 515 master

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!30

---

## 0.4.0 [06-26-2020]

* [minor/LB-404] Switch absolute path to relative path for img

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!28

---

## 0.3.0 [06-17-2020]

* Update manifest and readme to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Device-Pre-Post-Check-Execution!27

---

## 0.2.10 [05-05-2020]

* manifest changes

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!26

---

## 0.2.9 [05-01-2020]

* remove app artifacts from dependencies

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!25

---

## 0.2.8 [05-01-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!24

---

## 0.2.7 [05-01-2020]

* Update Readme.md

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!23

---

## 0.2.6 [04-17-2020]

* Update package.json [skip ci]

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!22

---

## 0.2.5 [04-17-2020]

* Update package.json [skip ci]

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!22

---

## 0.2.4 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!21

---

## 0.2.3 [03-20-2020]

* removed tags from manifest

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!20

---

## 0.2.2 [02-26-2020]

* Patch/20193 updates

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!18

---

## 0.2.1-2019.3.0 [01-23-2020]

* version bumping IAP dependencies to match release/2019.3

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!17

---

## 0.2.1 [01-16-2020]

* resize images and reformat markdown to align with linting rules

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!14

---

## 0.2.0 [08-06-2019]

* [minor/DSUP-685]Update IAPDependencies to use partial ranges rather than semver ranges

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!12

---
## 0.1.4 [08-01-2019]
* Delete itentialopensource-device-pre-post-check-execution-0.0.1.tgz

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!11

---

## 0.1.3 [08-01-2019]
* remove_pre-release_deploy from CI

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!10

---

## 0.1.2 [08-01-2019]
* Artifact matches current App-Artifacts version in package.json IAP dependencies

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!9

---

## 0.1.1 [07-31-2019]
* app-configuration_manager variation

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!7

---

## 0.1.0 [07-17-2019]
* [prepost]Update package.json to include App-Artifact keyword and IAPDep

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!6

---

## 0.0.5 [06-05-2019]
* renaming dependencies

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!4

---

## 0.0.4 [06-05-2019]
* Update package.json to include IAP dependencies

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!3

---

## 0.0.3 [05-29-2019]
* [patch/DSUP-623] Add requirements and complexity fields to package.json

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!2

---

## 0.0.2 [05-07-2019]
* init commit

See merge request itentialopensource/pre-built-automations/device-pre-post-check-execution!1

---

- [Device Pre-Post Check Execution](#device-pre-post-check-execution)
  - [Requirements](#Requirements)
  - [How to install](#how-to-install)
  - [How to run](#how-to-run)
- [Additional Information](#additional-information)

# Device Pre-Post Check Execution

The Device Pre-Post Check Execution artifact demonstrates an end-to-end usage of the MOP-app and Workflow in Itential Automation Platform (IAP).

<table><tr><td>
  <img src="./img/wf_shot.png" alt="workflow shot" width="800px">
</td></tr></table>

This artifact allows users to run the workflow in either **Simulated** or **Real-Time** mode.

For **Simulated** mode, this workflow doesn't have any prerequisites, and all responses are hard coded in the workflow.
In order to run this artifact in **real-time** mode, a Cisco-IOS device needs to be on-boarded on either Cisco-NSO or Ansible.

## Requirements

The following is required to run Artifact Wizard, and is compatible with the following versions:

- Itential Automation Platform
  - `^2021.1`

## How to install

In order to install this artifact, please use the App-Artifact (Available for Itential customers via Nexus repo)

## How to run

After installing (via App-Artifacts) click the start button on the artifact card or navigate to workflow builder and press the play button next to the MOP demo workflow.

Once started, the workflow will present the user with a question to either run in simulated or real-time mode:

<table><tr><td>
  <img src="./img/simulated-decision.png" alt="simulated question" width="600px">
</td></tr></table>

In **simulated mode**, this artifact works out of the box without any other dependencies. All device responses that are presented to the user are from a recorded session that were hard coded:

<table><tr><td>
  <img src="./img/pre-results.png" alt="Pre Checks Results" width="600px">
</td></tr></table>

<table><tr><td>
  <img src="./img/diff-results.png" alt="Diff Results" width="600px">
</td></tr></table>

In the **real-time mode**, the artifact will ask the user to pick a live device from their inventory of NSO or Ansible devices, it will then execute the commands on the selected device:

<table><tr><td>
  <img src="./img/device-picker.png" alt="Device Picker" width="600px">
</td></tr></table>

## Additional Information
Please use your Itential Customer Success account if you need support when using this artifact. 
